import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import React from "react";
import {ICON_MAIN_COLOR, ICON_MAIN_SIZE} from "../../styles";

const PressureIcon = <Icon
  name='heart-pulse'
  size={ICON_MAIN_SIZE}
  color={ICON_MAIN_COLOR}
/>;
export default PressureIcon