import React from "react";
import {Image} from 'react-native';

import Sugar from '../../images/icons8-sugar-monitor.png'
import {ICON_MAIN_COLOR, ICON_MAIN_SIZE} from "../../styles";

const SugarIcon = <Image
  style={{
    height: ICON_MAIN_SIZE,
    width: ICON_MAIN_SIZE
  }}
  source={Sugar}
/>;
export default SugarIcon