import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import React from "react";
import {ICON_MAIN_COLOR, ICON_MAIN_SIZE} from "../../styles";

const WeightIcon = <Icon
  name='weight-kilogram'
  size={ICON_MAIN_SIZE}
  color={ICON_MAIN_COLOR}
/>;
export default WeightIcon