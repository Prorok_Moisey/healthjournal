import Icon from 'react-native-vector-icons/FontAwesome5'
import React from "react";
import {ICON_MAIN_COLOR, ICON_MAIN_SIZE} from "../../styles";

const TemperatureIcon = <Icon
  name='temperature-low'
  size={ICON_MAIN_SIZE}
  color={ICON_MAIN_COLOR}
/>;
export default TemperatureIcon