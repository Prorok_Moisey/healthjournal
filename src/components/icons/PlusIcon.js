import Icon from 'react-native-vector-icons/AntDesign'
import React from 'react';
import {ICON_MAIN_COLOR, ICON_MINI_SIZE} from "../../styles";

const PlusIcon = <Icon
          style={{padding:15}}
          name='pluscircleo'
          size={ICON_MINI_SIZE}
          color={ICON_MAIN_COLOR}
        />;
export default PlusIcon;