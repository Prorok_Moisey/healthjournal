import Icon from 'react-native-vector-icons/Entypo'
import React, {Component} from 'react';
import {ICON_MAIN_COLOR, ICON_MAIN_SIZE} from "../../styles";
import {TouchableWithoutFeedback,View} from "react-native";
import { withNavigation } from 'react-navigation';

class MenuIcon extends Component {
  render() {
    return (
      <TouchableWithoutFeedback
        onPress={() => this.props.navigation.navigate('SettingsMenu')}
      >
        <View
          style={{
            padding: 10,
            paddingRight: 20,
          }}>
          <Icon
            name='menu'
            size={ICON_MAIN_SIZE}
            color={ICON_MAIN_COLOR}
          />
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

export default withNavigation(MenuIcon);
