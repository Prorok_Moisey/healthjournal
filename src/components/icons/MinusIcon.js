import Icon from 'react-native-vector-icons/AntDesign'
import React from "react";
import {DARK_GRAY_COLOR, ICON_MINI_SIZE} from "../../styles";

const MinusIcon = <Icon
  style={{padding:15}}
  name='minuscircleo'
  size={ICON_MINI_SIZE}
  color={DARK_GRAY_COLOR}
/>;
export default MinusIcon