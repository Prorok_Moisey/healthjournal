import Icon from 'react-native-vector-icons/FontAwesome'
import React, { Component } from "react";
import {ICON_MAIN_COLOR, ICON_MAIN_SIZE} from "../../styles";
import {TouchableWithoutFeedback, View} from "react-native";
import { withNavigation } from 'react-navigation';

class UserIcon extends Component {
  render() {
    return (
      <TouchableWithoutFeedback
        onPress={() => this.props.navigation.navigate('SettingsUserInfo')}
      >
        <View
          style={{
            padding: 7,
          }}>
          <Icon
            name='user'
            size={ICON_MAIN_SIZE}
            color={ICON_MAIN_COLOR}
          />
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

export default withNavigation(UserIcon);
