import React, {Component} from 'react';
import styled from 'styled-components'
import {LIGHT_GRAY_COLOR} from "../../styles";

const Container = styled.View`
  height: 50;
  width: 100%;
  border-bottom-width: 1;
  border-bottom-color: ${LIGHT_GRAY_COLOR};
  justify-content: center;
`;

const Input = styled.TextInput`
  font-size: 20;
`;

class UserInput extends Component {
  render() {
    return (
      <Container>
        <Input
          autoCapitalize='none'
          autoCorrect={false}
          {...this.props}
        />
      </Container>
    );
  }
}

export default UserInput