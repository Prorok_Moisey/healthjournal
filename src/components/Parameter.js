import React, { Component } from 'react';
import { TouchableWithoutFeedback} from 'react-native';
import styled from 'styled-components';
import {DARK_GRAY_COLOR, LIGHT_GRAY_COLOR, MAIN_TEXT_COLOR} from "../styles";

const Container = styled.View`
  width: 100%;
  height: ${props => props.big ? 60 : 50};
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  border-bottom-color:${LIGHT_GRAY_COLOR};
  border-bottom-width: 1;
`;

const IconView = styled.View`
  width: 40;
  height: 40;
  border: 1px solid black;
  borderRadius: 30;
  justify-content: center;
  align-items: center;
`;

const MainElement = styled.View`
  flex: 1;
  padding: 0 10px;
  height: 100%;
  justify-content: space-around;
  align-items: ${props => props.mode === 'center' ? 'center'
  : props.mode === 'right' ? 'flex-end' : 'flex-start'};
`;

const Title = styled.Text`
  font-size: 16;
  font-weight: bold;
  color: ${MAIN_TEXT_COLOR};
`;

const Secondary = styled.Text`
  font-size: 12px;
  font-weight: bold;
  color: ${DARK_GRAY_COLOR};
`;

const SideElement = styled.View`
  width: 50;
  height: 100%;
  justify-content: center;
  align-items: center;
`;

class Parameter extends Component {
  render() {
    return (
      <TouchableWithoutFeedback onPress={this.props.onPress && this.props.onPress}>
        <Container big={this.props.big}>
          <SideElement>
            { this.props.left &&
              <IconView>
                {this.props.left}
              </IconView>
            }
          </SideElement>
          <MainElement mode={this.props.position}>
            <Title>
              {this.props.big ?  this.props.main : this.props.main.toUpperCase()}
            </Title>
            { this.props.secondary &&
              <Secondary>{this.props.secondary}</Secondary>
            }
          </MainElement>
          <SideElement>
            {this.props.right && this.props.right}
          </SideElement>
        </Container>
      </TouchableWithoutFeedback>
    );
  }
}

export default Parameter;