import React, { Component } from 'react';
import { TouchableWithoutFeedback, View } from 'react-native';
import styled from 'styled-components';
import {LIGHT_GRAY_COLOR, MAIN_TEXT_COLOR} from "../styles";
import MinusIcon from './icons/MinusIcon'

const Container = styled.View`
  padding: 0 5px 0 20px;
  width: 100%;
  height: 50;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  border-bottom-color:${LIGHT_GRAY_COLOR};
  border-bottom-width: 1;
`;

const Title = styled.Text`
  padding: 20px 20px 20px 0;
  font-size: 15;
  color: ${MAIN_TEXT_COLOR};
`;

class MedicamentTime extends Component {
  render() {
    return (
      <Container>
        <TouchableWithoutFeedback
          onPress={this.props.onTimePress && this.props.onTimePress}
        >
          <Title>
            {this.props.time}
          </Title>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback
          onPress={this.props.onMinusPress && this.props.onMinusPress}
        >
            {MinusIcon}
        </TouchableWithoutFeedback>
      </Container>
    );
  }
}

export default MedicamentTime;