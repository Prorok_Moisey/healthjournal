import React, {Component} from 'react';
import styled from 'styled-components';

import {MAIN_THEME_COLOR, MAIN_TEXT_COLOR} from "../styles";
import UserIcon from "./icons/UserIcon"
import MenuIcon from "./icons/MenuIcon"
import BackIcon from "./icons/BackIcon"

const Container = styled.View`
  width: 100%;
  height: 50px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  background-color: ${MAIN_THEME_COLOR};
  border-bottom-color: black;
  border-bottom-width: 1;
`;

const Title = styled.Text`
  font-size: 16;
  color: ${MAIN_TEXT_COLOR};
  font-weight: bold;
`;

const IconContainer = styled.View`
  height: 50;
  width: 50;
`;

class Header extends Component {
  render() {
    return (
      <Container>
        <IconContainer>
          <MenuIcon/>
        </IconContainer>
        { this.props.title &&
          <Title>
            {this.props.title}
          </Title>
        }
        <IconContainer>
          <UserIcon/>
        </IconContainer>
      </Container>
    );
  }
}

export default Header;