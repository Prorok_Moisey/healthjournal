import React, { Component } from 'react';
import styled from 'styled-components';
import {SECONDARY_THEME_COLOR, MAIN_TEXT_COLOR, ICON_BIG_SIZE, ICON_MAIN_COLOR, ICON_MAIN_SIZE} from "../styles";
import EntypoIcon from 'react-native-vector-icons/Entypo';
import IoniconsIcon from 'react-native-vector-icons/Ionicons';
import {Image, TouchableWithoutFeedback, View} from "react-native";

const Container = styled.View`
  width: 100%;
  height: 60;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const TitleView = styled.View`
  flex: 1;
  padding: 0 10px;
  height: 100%;
  justify-content: space-around;
  align-items: ${props => props.mode === 'center' ? 'center'
  : props.mode === 'right' ? 'flex-end' : 'flex-start'};
`;

const MainTitle = styled.Text`
  font-size: 16;
  color: ${MAIN_TEXT_COLOR};
`;

const SecondaryTitle = styled.Text`
  font-size: 16px;
  color: ${SECONDARY_THEME_COLOR};
`;

const IconContainer = styled.View`
  flex: 1;
  height: 100%;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
`;

const IconView = styled.View`
  width: 40;
  height: 40;
  border: 1px solid ${props => props.focused ? SECONDARY_THEME_COLOR : 'black'};
  borderRadius: 30;
  justify-content: center;
  align-items: center;
`;

class ParameterInputState extends Component {
  render() {
    return (
      <Container>
        <TitleView mode={this.props.position}>
          <MainTitle>
            {this.props.main.charAt(0).toUpperCase()+this.props.main.slice(1)}
          </MainTitle>
          <SecondaryTitle>
            {this.props.state === '' ? 'Не указано' : this.props.state.charAt(0).toUpperCase()+this.props.state.slice(1)}
          </SecondaryTitle>
        </TitleView>
        <IconContainer>
          { this.props.icons &&
            this.props.icons.map(item => (
              <TouchableWithoutFeedback onPress={() => this.props.onPress(item.state)}>
                { item.type === 'Entypo'
                  ? <EntypoIcon
                      name={item.name}
                      size={ICON_BIG_SIZE}
                      color={this.props.state === item.state ? SECONDARY_THEME_COLOR : ICON_MAIN_COLOR}
                    />
                  : item.type === 'Ionicons'
                    ? <IconView focused={this.props.state === item.state}>
                        <IoniconsIcon
                          name={item.name}
                          size={ICON_MAIN_SIZE}
                          color={this.props.state === item.state ? SECONDARY_THEME_COLOR : ICON_MAIN_COLOR}
                        />
                      </IconView>
                    : item.type === 'image'
                      ? <IconView focused={this.props.state === item.state}>
                          <Image
                            style={{
                              height: ICON_MAIN_SIZE,
                              width: ICON_MAIN_SIZE
                            }}
                            source={this.props.state === item.state ? item.sourceFocused : item.source}
                          />
                        </IconView>
                      : <View style={{width: 40}}/>
                }
              </TouchableWithoutFeedback>
            ))
          }
        </IconContainer>
      </Container>
    );
  }
}

export default ParameterInputState;