//l ibraries
import React, { Component } from 'react';
import styled from 'styled-components';
import {TouchableWithoutFeedback} from 'react-native';

// components
import {LIGHT_GRAY_COLOR, MAIN_TEXT_COLOR, SECONDARY_THEME_COLOR} from "../styles";

const Container = styled.View`
  width: 100%;
  padding: 0 20px;
  height: 50;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  border-bottom-color:${LIGHT_GRAY_COLOR};
  border-bottom-width: 1;
`;

const Title = styled.Text`
  font-size: 16;
  color: ${SECONDARY_THEME_COLOR};
`;

const Input = styled.TextInput`
  flex: 1;
  font-size: 16;
  text-align: right;
  color: ${MAIN_TEXT_COLOR};
`;

class MedicamentInput extends Component {
  setRef = (ref) => {this.input = ref};
  render() {
    return (
      <TouchableWithoutFeedback
        onPress={() => this.input.focus()}
      >
        <Container>
          <Title>
            {this.props.title[0].toUpperCase()+this.props.title.slice(1)}
          </Title>
          <Input
            ref={this.setRef}
            placeholder={this.props.placeholder}
            value={this.props.value}
            onChangeText={(text) => this.props.onChangeText(text)}
          />
        </Container>
      </TouchableWithoutFeedback>
    );
  }
}

export default MedicamentInput;