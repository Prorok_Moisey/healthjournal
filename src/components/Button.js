import React, {Component} from 'react';
import {TouchableWithoutFeedback} from 'react-native';
import styled from 'styled-components'
import {MAIN_THEME_COLOR, SECONDARY_THEME_COLOR} from "../styles";

const ButtonView = styled.View`
  height: 40;
  flex: 1;
  max-width: ${props => props.little ? '150' : '180'};
  align-items: center;
  justify-content: center;
  background-color: ${MAIN_THEME_COLOR};
  border: 1px solid ${SECONDARY_THEME_COLOR};
  border-radius: 15px;
`;

const Title = styled.Text`
  font-size: 16;
  color: ${SECONDARY_THEME_COLOR};
`;

class Button extends Component {
  render() {
    return (
      <TouchableWithoutFeedback
        onPress={this.props.onPress ? this.props.onPress : null}
      >
        <ButtonView little={this.props.little}>
          <Title>
            {this.props.title.toUpperCase()}
          </Title>
        </ButtonView>
      </TouchableWithoutFeedback>
    );
  }
}

export default Button