import { AsyncStorage } from "react-native";

//TODO:  https://github.com/zalmoxisus/remote-redux-devtools#enabling
import { composeWithDevTools } from "remote-redux-devtools";

import { createStore, applyMiddleware } from "redux";
import { persistStore, persistCombineReducers } from "redux-persist";
import immutableTransform from "redux-persist-transform-immutable";

import reducers from "./state/rootReducer";
import createSagaMiddleware from "redux-saga";
// import saga from "./state/rootSaga";
//import analyticsMiddleware from "./state/analyticsMiddleware";
//import Reactotron from "./reactotronConfig";

let _store;
let _persistor;

function configureStore(onComplete) {
  const appReducer = persistCombineReducers(
    {
      key: "root",
      storage: AsyncStorage,
//      blacklist: ["forms"],
    },
    reducers
  );

  // const sagaMiddleware = createSagaMiddleware();
  const store = createStore(
    appReducer, {},
    // composeWithDevTools(applyMiddleware(analyticsMiddleware, sagaMiddleware), Reactotron.createEnhancer())
    // composeWithDevTools(applyMiddleware(sagaMiddleware))
  );

  // if(module.hot){222
  //     module.hot.accept('./state/reducers', () => {
  //         const nextRootReducer = require('./state/mainReducer');
  //         store.replaceReducer(nextRootReducer);
  //     });
  // }

  //sagaMiddleware.run(saga);
  const persistor = persistStore(store, null, () => {
    store.dispatch({ type: "READY" });
  });
  _store = store;
  _persistor = persistor;
  return { store, persistor };
}

const { store, persistor } = configureStore();

export { store, persistor };

// export function getStore() {
//   return store;
// }
