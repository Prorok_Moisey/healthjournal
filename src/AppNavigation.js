// libraries
import React, {Component} from 'react';
import {View, TouchableWithoutFeedback} from 'react-native';
import {createStackNavigator, createAppContainer, createMaterialTopTabNavigator} from 'react-navigation'
import styled from 'styled-components'

// components
import MainScreen from "./screens/MainScreen";
import MedicamentScreen from "./screens/MedicamentScreen";
import JournalScreen from "./screens/JournalScreen";
import WeightAddingScreen from "./screens/noteScreens/WeightAddingScreen"
import TemperatureAddingScreen from "./screens/noteScreens/TemperatureAddingScreen";
import PressureAddingScreen from "./screens/noteScreens/PressureAddingScreen";
import SugarAddingScreen from "./screens/noteScreens/SugarAddingScreen";
import MedicamentAddingScreen from "./screens/noteScreens/MedicamentAddingScreen";
import SettingsParametersScreen from "./screens/SettingsParametersScreen";
import SettingsMenuScreen from "./screens/SettingsMenuScreen";
import SettingsUserInfoScreen from "./screens/SettingsUserInfoScreen";
import { MAIN_TEXT_COLOR, MAIN_THEME_COLOR } from "./styles";
import UserIcon from "./components/icons/UserIcon"
import MenuIcon from "./components/icons/MenuIcon";
import BackIcon from "./components/icons/BackIcon";
import {connect, Provider} from "react-redux";

// constants
const TopBarText = styled.Text`
  flex: 1;
  height: 100%;
  width: 100%;
  font-size: 12px;
  font-weight: ${props => props.focused ? 'bold' : 'normal'};
  text-align-vertical: center;
  text-align: center;
`;

class AppNavigator extends Component {
  render() {

    const ParameterStack = createStackNavigator(
      {
        ParameterMain: MainScreen,
        WeightAdding: WeightAddingScreen,
        PressureAdding: PressureAddingScreen,
        TemperatureAdding: TemperatureAddingScreen,
        SugarAdding: SugarAddingScreen,
      },
      {
        initialRouteName: 'ParameterMain',
        defaultNavigationOptions: {
          header: null
        }
      }
    );

    const MedicamentStack = createStackNavigator(
      {
        MedicamentMain: MedicamentScreen,
        MedicamentAdding: MedicamentAddingScreen,
        MedicamentEditing: MedicamentAddingScreen,
      },
      {
        initialRouteName: 'MedicamentMain',
        defaultNavigationOptions: {
          header: null
        }
      }
    );

    const JournalStack = createStackNavigator(
      {
        JournalMain: JournalScreen,
        WeightEditing: WeightAddingScreen,
        PressureEditing: PressureAddingScreen,
        TemperatureEditing: TemperatureAddingScreen,
        SugarEditing: SugarAddingScreen,
      },
      {
        initialRouteName: 'JournalMain',
        defaultNavigationOptions: {
          header: null
        }
      }
    );


    // const SettingsStack = createStackNavigator(
    //   {
    //     SettingsMenu: SettingsMenuScreen,
    //     SettingsParameters: SettingsParametersScreen,
    //     SettingsUserInfo: SettingsUserInfoScreen
    //   },
    //   {
    //     initialRouteName: 'SettingsMenu',
    //     defaultNavigationOptions: {
    //       header: null
    //     }
    //   }
    // );

    const TopNavigator = createMaterialTopTabNavigator(
      {
        Parameters: ParameterStack,
        Medicament: MedicamentStack,
        Journal: JournalStack,
      },
      {
        initialRouteName: 'Parameters',
        defaultNavigationOptions: ({ navigation }) => ({
          tabBarLabel: ({ focused }) =>{
            const { routeName } = navigation.state;
            const icon =
              routeName === 'Parameters' ? (
                <TouchableWithoutFeedback
                  onPress={() => {
                    this.props.navigation.navigate("Parameters");
                  }}
                >
                  <View
                    style={{
                      width: "100%",
                      justifyContent: "center",
                      alignItems: "center",
                      flex: 1,
                    }}
                  >
                    <TopBarText focused={focused}>ВВОД ДАННЫХ</TopBarText>
                  </View>
                </TouchableWithoutFeedback>
              ) : routeName === 'Medicament' ? (
                <TouchableWithoutFeedback
                  onPress={() => {
                    this.props.navigation.navigate("Parameters");
                  }}
                >
                  <View
                    style={{
                      width: "100%",
                      justifyContent: "center",
                      alignItems: "center",
                      flex: 1,
                    }}
                  >
                    <TopBarText focused={focused}>ПРИЁМ ЛЕКАРСТВ</TopBarText>
                  </View>
                </TouchableWithoutFeedback>
              ) : (
                <TouchableWithoutFeedback
                  onPress={() => {
                    this.props.navigation.navigate("Parameters");
                  }}
                >
                  <View
                    style={{
                      width: "100%",
                      justifyContent: "center",
                      alignItems: "center",
                      flex: 1,
                    }}
                  >
                    <TopBarText focused={focused}>ЖУРНАЛ</TopBarText>
                  </View>
                </TouchableWithoutFeedback>
              );
            return React.cloneElement(icon);
          }
        }),
        tabBarOptions: {
          style: {
            backgroundColor: 'white',
          },
          indicatorStyle: {
            backgroundColor: MAIN_THEME_COLOR,
            height: '100%',
            borderRightWidth: 1,
            borderLeftWidth: 1
          },
          tabStyle: {
            borderBottomWidth: 1,
          }
        },
      }
    );

    const RootStack = createStackNavigator(
      {
        Main: TopNavigator,
        SettingsMenu: SettingsMenuScreen,
        SettingsParameters: SettingsParametersScreen,
        SettingsUserInfo: SettingsUserInfoScreen,
      },
      {
        // mode: 'modal',
        defaultNavigationOptions: {
          headerTitle: this.props.firstName !==''
            ? this.props.lastName !==''
              ? this.props.lastName.toUpperCase() + ' ' + this.props.firstName.toUpperCase()
              : this.props.firstName.toUpperCase()
            : this.props.lastName !==''
              ? this.props.lastName.toUpperCase()
              : 'ИМЯ ПОЛЬЗОВАТЕЛЯ',
          headerStyle: {
            backgroundColor: MAIN_THEME_COLOR,
            borderBottomColor: 'black',
            borderBottomWidth: 1,
          },
          headerTitleStyle: {
            fontSize: 16,
            color: MAIN_TEXT_COLOR,
            fontWeight: 'bold',
            textAlign: 'center',
            flex: 1
          },
          headerRight: <UserIcon/>,
          headerRightContainerStyle: {
            margin: 9,
            borderWidth: 1,
            borderRadius: 20
          },
          headerLeft: <MenuIcon/>,
        }
      }
    );

    TopNavigator.navigationOptions = ({navigation}) => {
      // const { routeName } = navigation.state.routes[navigation.state.index];
      const { routeName } = navigation.state.routes[navigation.state.index].routes
        ? navigation.state.routes[navigation.state.index].routes[navigation.state.routes[navigation.state.index].routes.length-1]
        : navigation.state.routes[navigation.state.index];
      if (routeName === 'WeightAdding' ||
        routeName === 'PressureAdding' ||
        routeName === 'SugarAdding' ||
        routeName === 'TemperatureAdding'){
        const header ={
          headerTitle: 'НОВОЕ ИЗМЕРЕНИЕ',
          headerLeft: <BackIcon/>,
          headerRight: <View/>,
          headerRightContainerStyle: {},
        };
        return header;
      } else if (routeName === 'MedicamentAdding'){
        const header ={
          headerTitle: 'НОВЫЙ ПРЕПАРАТ',
          headerLeft: <BackIcon/>,
          headerRight: <View/>,
          headerRightContainerStyle: {},
        };
        return header;
      } else if (routeName === 'WeightEditing' ||
        routeName === 'PressureEditing' ||
        routeName === 'SugarEditing' ||
        routeName === 'TemperatureEditing' ||
        routeName === 'MedicamentEditing'){
        const header ={
          headerTitle: 'РЕДАКТИРОВАНИЕ',
          headerLeft: <BackIcon/>,
          headerRight: <View/>,
          headerRightContainerStyle: {},
        };
        return header;
      }
    };

    const AppNavigation = createAppContainer(RootStack);

    return (
        <AppNavigation firstName={this.props.firstName}/>
    );
  }
}

const mapStateToProps = state =>({
  firstName: state.user.firstName,
  lastName: state.user.lastName
});

export default connect (
  mapStateToProps,
)(AppNavigator);

//TODO сделать более простым изменение хэдера при наличии имени пользователя
// без использования вынесения навигаторов в отдельный компонент

