// libraries
import React, {Component} from 'react';
import {View} from 'react-native';
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/lib/integration/react";
import { store, persistor } from "./configureStore";

import AppNavigator from './AppNavigation'

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <View style={{flex:1}}>
            <AppNavigator/>
          </View>
        </PersistGate>
      </Provider>
    );
  }
}

export default App