import { takeLatest, call, put, select } from "redux-saga/effects";
import {ACTION_USER_INFO_SAVE, userUpdate} from "../reducers/user";

function* userSetInfo (user) {
  try {
    yield put(userUpdate(user));
  } catch (error){
    yield console.log(error);
  }
}

export default [
  takeLatest(ACTION_USER_INFO_SAVE, userSetInfo),
];