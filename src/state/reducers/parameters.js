// Dependencies
import immutableTransform from "redux-persist-transform-immutable";

// images
import Bed from "../../images/icons8-bed.png";
import BedFocused from "../../images/icons8-bed-focused.png";
import Chair from "../../images/icons8-chair.png";
import ChairFocused from "../../images/icons8-chair-focused.png";
import LeftHand from "../../images/icons8-left-hand.png";
import LeftHandFocused from "../../images/icons8-left-hand-focused.png";
import RightHand from "../../images/icons8-right-hand.png";
import RightHandFocused from "../../images/icons8-right-hand-focused.png";

// Actions
export const ACTION_PARAMETER_VISIBILITY_CHANGE = 'health_journal/parameters/visibility/change';
export const ACTION_PARAMETER_ADD = 'health_journal/parameters/parameter/add';
export const ACTION_PARAMETER_DELETE = 'health_journal/parameters/parameter/delete';

// State
const initialState = {
  feelingIcons:[
    {name: 'emoji-sad', type: 'Entypo', state: 'плохо'},
    {name: 'emoji-neutral', type: 'Entypo', state: 'нормально'},
    {name: 'emoji-happy', type: 'Entypo', state: 'хорошо'}
  ],
  handIcons:[
    {type: 'none'},
    {source: LeftHand, sourceFocused: LeftHandFocused, type: 'image', state: 'левая'},
    {source: RightHand, sourceFocused: RightHandFocused, type: 'image', state: 'правая'},
  ],
  bodyIcons:[
    {name: 'ios-man', type: 'Ionicons', state: 'стоя'},
    {source: Chair, sourceFocused: ChairFocused, type: 'image', state: 'сидя'},
    {source: Bed, sourceFocused: BedFocused, type: 'image', state: 'лёжа'},
  ],
  weightEnabled: true,
  pressureEnabled: true,
  temperatureEnabled: true,
  sugarEnabled: true,
  parameterList: [],
};

// Persist Configuration
export const persistConfig = {
  // transforms: [immutableTransform()],
  key: "parameters",
  // blacklist: ["firstName", "lastName", 'birthDate', 'sex', 'growth'],
};

//Reducer
export function reducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_PARAMETER_VISIBILITY_CHANGE:
      return{
        ...state,
        weightEnabled: action.visibility.hasOwnProperty('weightEnabled') ? action.visibility.weightEnabled : state.weightEnabled,
        pressureEnabled: action.visibility.hasOwnProperty('pressureEnabled') ? action.visibility.pressureEnabled : state.pressureEnabled,
        temperatureEnabled: action.visibility.hasOwnProperty('temperatureEnabled') ? action.visibility.temperatureEnabled : state.temperatureEnabled,
        sugarEnabled: action.visibility.hasOwnProperty('sugarEnabled') ? action.visibility.sugarEnabled : state.sugarEnabled,
      };
    case ACTION_PARAMETER_ADD:
      const index = state.parameterList.findIndex(elem => elem.id === action.parameter.id);
      const newParameterList = index === -1
        ? state.parameterList.concat(action.parameter)
        : state.parameterList.map(item => {
            if (item.id === action.parameter.id) {
              return action.parameter
            } else {return item}
          });
      return{
        ...state,
        parameterList: newParameterList,
        // parameterList: state.parameterList.concat(action.parameter),
      };
    case ACTION_PARAMETER_DELETE:
      return{
        ...state,
        parameterList: state.parameterList.filter(item => item.id !== action.parameterId)
      };
    default:
      return { ...state };
  }
}

// region <--== Parameter Visibility Change ==-->
export function parameterVisibilityChange(visibility) {
  return {type: ACTION_PARAMETER_VISIBILITY_CHANGE, visibility: visibility}
}
// end_region

// region <--== Parameter Add ==-->
export function parameterAdd(parameter) {
  return {type: ACTION_PARAMETER_ADD, parameter: parameter}
}
// end_region

// region <--== Parameter Delete ==-->
export function parameterDelete(parameterId) {
  return {type: ACTION_PARAMETER_DELETE, parameterId: parameterId}
}
// end_region