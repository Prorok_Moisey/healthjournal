// Dependencies
import immutableTransform from "redux-persist-transform-immutable";

// Constants
// const reducerName = 'user;';

// Actions
export const ACTION_USER_INFO_SAVE = 'health_journal/user/info/save';
// export const userActions = {
//   save: 'health_journal/user/save_info'
// };

// State
const initialState = {
  firstName: '',
  lastName: '',
  birthDate: '',
  sex: '',
  growth: '',
};

// Persist Configuration
export const persistConfig = {
  // transforms: [immutableTransform()],
  key: "user",
  // blacklist: ["firstName", "lastName", 'birthDate', 'sex', 'growth'],
};

//Reducer
export function reducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_USER_INFO_SAVE:
      return{
        ...state,
        firstName: action.user.hasOwnProperty('firstName') ? action.user.firstName : state.firstName,
        lastName: action.user.hasOwnProperty('lastName') ? action.user.lastName : state.lastName,
        birthDate: action.user.hasOwnProperty('birthDate') ? action.user.birthDate : state.birthDate,
        sex: action.user.hasOwnProperty('sex') ? action.user.sex : state.sex,
        growth: action.user.hasOwnProperty('growth') ? action.user.growth : state.growth,
      };
    default:
      return { ...state };
  }
}

// region <--== User Info Update ==-->
export function userUpdate(user) {
  // return {type: userActions.save, user}
  return {type: ACTION_USER_INFO_SAVE, user: user}
}
// end_region