// Dependencies
import immutableTransform from "redux-persist-transform-immutable";


// Actions
export const ACTION_MEDICAMENT_ADD = 'health_journal/medicaments/medicament/add';
export const ACTION_MEDICAMENT_DELETE = 'health_journal/medicaments/medicament/delete';

// State
const initialState = {
  medicamentList: [],
};

// Persist Configuration
export const persistConfig = {
  // transforms: [immutableTransform()],
  key: "medicaments",
  // blacklist: ["firstName", "lastName", 'birthDate', 'sex', 'growth'],
};

//Reducer
export function reducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_MEDICAMENT_ADD:
      const index = state.medicamentList.findIndex(elem => elem.id === action.medicament.id);
      const newMedicamentList = index === -1
        ? state.medicamentList.concat(action.medicament)
        : state.medicamentList.map(item => {
          if (item.id === action.medicament.id) {
            return action.medicament
          } else {return item}
        });
      return{
        ...state,
        medicamentList: newMedicamentList,
        // medicamentList: state.medicamentList.concat(action.medicament),
      };
    case ACTION_MEDICAMENT_DELETE:
      return{
        ...state,
        medicamentList: state.medicamentList.filter(item => item.id !== action.medicamentId)
      };
    default:
      return { ...state };
  }
}

// region <--== Medicament Add ==-->
export function medicamentAdd(medicament) {
  return {type: ACTION_MEDICAMENT_ADD, medicament: medicament}
}
// end_region

// region <--== Medicament Delete ==-->
export function medicamentDelete(medicamentId) {
  return {type: ACTION_MEDICAMENT_DELETE, medicamentId: medicamentId}
}
// end_region