import { AsyncStorage } from 'react-native';
import { persistReducer } from 'redux-persist';

import { persistConfig as userConfig, reducer as userReducer } from './reducers/user';
import { persistConfig as parametersConfig, reducer as parametersReducer } from './reducers/parameters';
import { persistConfig as medicamentsConfig, reducer as medicamentsReducer } from './reducers/medicaments';

const setStorage = config => Object.assign({}, config, { storage: AsyncStorage });

export default {
  user: persistReducer(setStorage(userConfig), userReducer),
  parameters: persistReducer(setStorage(parametersConfig), parametersReducer),
  medicaments: persistReducer(setStorage(medicamentsConfig), medicamentsReducer),
}