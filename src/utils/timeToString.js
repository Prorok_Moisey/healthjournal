export default (time) => {
  return ( time.getHours().toString().length === 1 ? '0'+time.getHours() : time.getHours() ) + ':'
    + ( time.getMinutes().toString().length === 1 ? '0'+time.getMinutes() : time.getMinutes() );
}