export default (date) => {
  const dateArr = date.toString().split('/');
  return [dateArr[1], dateArr[0], dateArr[2],].join('/')
}