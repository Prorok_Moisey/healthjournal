// libraries
import React, {Component} from 'react';
import {ScrollView, View, TouchableWithoutFeedback} from 'react-native';
import {connect} from "react-redux";
import styled from "styled-components";

// components
import Parameter from "../components/Parameter";
import {DARK_GRAY_COLOR, LIGHT_GRAY_COLOR, MAIN_TEXT_COLOR} from "../styles";

const InfoText = styled.Text`
  color: ${DARK_GRAY_COLOR};
  font-size: 18px;
  text-align: center;
  border-bottom-color:${LIGHT_GRAY_COLOR};
  border-bottom-width: 1;
  padding-bottom: 10px;
`;

const Container = styled.View`
  width: 100%;
  height: ${props => props.big ? 60 : 50};
  flex-direction: row;
  justify-content: center;
  align-items: center;
  border-bottom-color:${LIGHT_GRAY_COLOR};
  border-bottom-width: 1;
`;

const Title = styled.Text`
  font-size: 16;
  font-weight: bold;
  color: ${MAIN_TEXT_COLOR};
`;

class MedicamentScreen extends Component {
  render() {
    return (
      <View style={{ alignItems: 'center', flex: 1 }}>
        <ScrollView
          style={{width: '100%', flex: 1}}
          contentContainerStyle={{alignItems: 'center'}}
        >
          <View style={{width: '90%'}}>
            { this.props.medicamentList.length !== 0 ?
              this.props.medicamentList.map(item => (
                <Parameter
                  main={item.name}
                  position='center'
                  onPress={() =>
                    this.props.navigation.navigate(item.routeName, {medicament: item})
                  }
                />
              ))
              : <View style={{paddingTop: 150}}>
                <TouchableWithoutFeedback
                  onPress={() => this.props.navigation.navigate('ParameterMain')}
                >
                  <InfoText>У Вас сейчас нет сохранённых напоминаний о приёме лекарств</InfoText>
                </TouchableWithoutFeedback>
              </View>
            }
            <TouchableWithoutFeedback
              onPress={() => this.props.navigation.navigate('MedicamentAdding')}
            >
              <Container>
                <Title>
                  + ДОБАВИТЬ НАПОМИНАНИЕ
                </Title>
              </Container>
            </TouchableWithoutFeedback>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state =>({
  medicamentList: state.medicaments.medicamentList,
});

export default connect (
  mapStateToProps,
)(MedicamentScreen);