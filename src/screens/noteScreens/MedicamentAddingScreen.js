// libraries
import React, {Component} from 'react';
import {ScrollView, View, TouchableWithoutFeedback} from 'react-native';
import styled from "styled-components";
import DateTimePicker from "react-native-modal-datetime-picker";
import PlusIcon from 'react-native-vector-icons/AntDesign'
import moment from "moment";
import _ from "lodash";
import {connect} from "react-redux";

// components
import {DARK_GRAY_COLOR, ICON_MINI_SIZE, LIGHT_GRAY_COLOR, SECONDARY_THEME_COLOR} from "../../styles";
import MedicamentInput from "../../components/MedicamentInput";
import MedicamentTime from "../../components/MedicamentTime";
import Button from "../../components/Button";

// state
import {medicamentAdd, medicamentDelete} from "../../state/reducers/medicaments";

const SectionTitle = styled.Text`
  height: 30;
  width: 100%;
  color: ${SECONDARY_THEME_COLOR};
  font-size: 14px;
  text-align-vertical: bottom;
  padding-left: 20;
  padding-bottom: 3;
  border-bottom-color: ${SECONDARY_THEME_COLOR};
  border-bottom-width: 2px;
`;

const AddMedicamentButton = styled.View`
  padding: 0 5px 0 20px;
  width: 100%;
  height: 50;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  border-bottom-color:${LIGHT_GRAY_COLOR};
  border-bottom-width: 1;
`;

const Text = styled.Text`
  padding-bottom: 5px;
  font-size: 14;
  color: ${DARK_GRAY_COLOR};
`;

const TimeSection = styled.View`
  minHeight: 250;
  width: 100%;
`;

const ButtonContainer = styled.View`
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  padding: 45px 0;
  width: 100%;

`;

class MedicamentAddingScreen extends Component {
  constructor(props) {
    super(props);
    const medicament = this.props.navigation.getParam('medicament', null);
    this.state={
      id: medicament ? medicament.id : new Date().valueOf(),
      name: medicament ? medicament.name : '',
      dosage: medicament ? medicament.dosage : '',
      times: medicament ? medicament.times : [],
      isTimePickerVisible: false,
      changeTimeId: '',
    }
  }

  showTimePicker = () => {
    this.setState({ isTimePickerVisible: true });
  };

  hideTimePicker = () => {
    this.setState({ isTimePickerVisible: false });
  };

  handleTimePicked = time => {
    const pickTime = moment(time).format('HH:mm');
    const newTimes = this.state.times.map(item => {
      if (item.id === this.state.changeTimeId) {
        item['time'] = pickTime;
        return item;
      } else {
        return item
      }
    });
    this.setState({ times: newTimes, changeTimeId: '' });
    this.hideTimePicker();
  };

  render() {
    return (
      <View style={{alignItems: 'center', flex: 1}}>
        <ScrollView
          style={{width: '100%', flex: 1}}
          contentContainerStyle={{alignItems: 'center'}}
        >
          <View style={{width: '90%'}}>
            <MedicamentInput
              title='название'
              placeholder='название препарата'
              value={this.state.name}
              onChangeText={
                (text) => this.setState({name: text})
              }
            />
            <MedicamentInput
              title='дозировка'
              placeholder='доза приёма'
              value={this.state.dosage}
              onChangeText={
                (text) => this.setState({dosage: text})
              }
            />
            <SectionTitle>Время приёма</SectionTitle>
            <TimeSection>
              {
                _.orderBy(this.state.times, ['time'],['asc']).map( item => (
                  <MedicamentTime
                    time={item.time}
                    onTimePress={() => {
                      this.setState({changeTimeId: item.id});
                      this.showTimePicker();
                    }}
                    onMinusPress={() => {
                      this.setState({times: this.state.times.filter(elem => elem.id !== item.id)})
                    }}
                  />
                ))
              }
              <AddMedicamentButton>
                <TouchableWithoutFeedback
                  onPress={() => {
                    const newTimes = this.state.times.concat(
                      {
                        id: new Date().valueOf(),
                        time: moment().format('HH:mm'),
                      });
                    this.setState({times: newTimes})
                  }}
                >
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <PlusIcon
                      style={{margin:10}}
                      name='pluscircleo'
                      size={ICON_MINI_SIZE}
                      color={DARK_GRAY_COLOR}
                    />
                    <Text>
                      добавить время приёма
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
              </AddMedicamentButton>
            </TimeSection>
          </View>
          <ButtonContainer>
            { this.props.navigation.getParam('medicament', null) &&
            <Button
              little
              title='удалить'
              onPress={() => {
                this.props.onDelete(this.state.id);
                this.props.navigation.goBack();
              }}
            />
            }
            <Button
              little={this.props.navigation.getParam('medicament', null) ? true : false}
              title='сохранить'
              onPress={() => {
                const medicament = {
                  id: this.state.id,
                  name: this.state.name,
                  dosage: this.state.dosage,
                  times: this.state.times,
                  routeName: 'MedicamentEditing',
                };
                this.props.onSave(medicament);
                this.props.navigation.goBack()
              }}
            />
          </ButtonContainer>
        </ScrollView>
        <DateTimePicker
          isVisible={this.state.isTimePickerVisible}
          onConfirm={this.handleTimePicked}
          onCancel={this.hideTimePicker}
          mode='time'
        />
      </View>
    );
  }
}

const mapDispatchToProps = dispatch =>({
  onSave: (values) => {
    dispatch(
      medicamentAdd(values)
    )
  },
  onDelete: (id) => {
    dispatch(
      medicamentDelete(id)
    )
  }
});

export default connect (
  null,
  mapDispatchToProps
)(MedicamentAddingScreen);