// libraries
import React, {Component} from 'react';
import {View, ScrollView, TouchableWithoutFeedback} from 'react-native';
import styled from 'styled-components'
import Icon from 'react-native-vector-icons/MaterialIcons';
import DateTimePicker from 'react-native-modal-datetime-picker'
import {connect} from "react-redux";
import moment from "moment";

// components
import {DARK_GRAY_COLOR, MAIN_TEXT_COLOR} from "../../styles";
import ParameterInputState from "../../components/ParameterInputState";
import Button from "../../components/Button";

// utils
import dateToString from "../../utils/dateToString";

// state
import {parameterAdd, parameterDelete} from "../../state/reducers/parameters";

const InputSection = styled.View`
  margin: 20px 0;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const BigInput = styled.TextInput`
  color: ${MAIN_TEXT_COLOR};
  font-size: 48px;
  width: 130;
  text-align: right;
`;
const Postfix = styled.Text`
  color: ${MAIN_TEXT_COLOR};
  font-size: 48px;
`;

const TimeSection = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
`;

const TimeElement = styled.View`
  padding: 15px 5px 15px 5px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const Time = styled.Text`
  font-size: 14px;
  width: 38;
  text-align:right;
`;

const DateView = styled.Text`
  font-size: 14px;
  width: 80;
  text-align:right;
`;

const StatesSection = styled.View`
  flex-direction: row;
  justify-content: space-around;
  minHeight: 190;
  width: 90%;
`;

const ButtonContainer = styled.View`
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  padding: 50px 0;
`;

class TemperatureAddingScreen extends Component {
  constructor(props){
    super(props);
    const parameter = this.props.navigation.getParam('parameter', null);
    this.state={
      id: parameter ? parameter.id :  new Date().valueOf(),
      value: parameter ? parameter.value : '',
      time: parameter ? parameter.time :  moment().format('HH:mm'),
      date: parameter ? parameter.date :  moment().format('L'),
      feeling: parameter ? parameter.feeling :  '',
      mode: 'time',
      isDateTimePickerVisible: false,
    }
  }

  showDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: true });
  };

  hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false });
  };

  handleTimePicked = time => {
    const pickTime = moment(time).format('HH:mm');
    this.setState({time: pickTime});
    this.hideDateTimePicker();
  };
  handleDatePicked = date => {
    const pickDate = moment(date).format('L');
    this.setState({date: pickDate});
    this.hideDateTimePicker();
  };

  render() {
    return (
      <View style={{alignItems: 'center', flex: 1}}>
        <ScrollView style={{width: '100%', flex: 1}}>
          <View style={{width: '100%', alignItems: 'center'}}>
            <InputSection>
              <BigInput
                placeholder='. . .'
                keyboardType='numeric'
                maxLength={4}
                value={this.state.value}
                onChangeText={
                  (text) => this.setState({value: text})
                }
              />
              <Postfix>°C</Postfix>
            </InputSection>
            <TimeSection>
              <TouchableWithoutFeedback  onPress={() =>{
                this.setState({mode: 'time'});
                this.showDateTimePicker();
              }}>
                <TimeElement>
                  <Time>{this.state.time.toString()}</Time>
                  <Icon name='navigate-next' size={20} color={DARK_GRAY_COLOR}/>
                </TimeElement>
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback onPress={() =>{
                this.setState({mode: 'date'});
                this.showDateTimePicker();
              }}>
                <TimeElement>
                  <DateView>{dateToString(this.state.date)}</DateView>
                  <Icon name='navigate-next' size={20} color={DARK_GRAY_COLOR}/>
                </TimeElement>
              </TouchableWithoutFeedback>
            </TimeSection>
            <StatesSection>
              <ParameterInputState
                main='самочувствие'
                state={this.state.feeling}
                icons={this.props.feelingIcons}
                onPress={
                  (state) => this.setState({feeling: this.state.feeling === state ? '' : state})
                }
              />
            </StatesSection>
          </View>
          <ButtonContainer>
            { this.props.navigation.getParam('parameter', null) &&
              <Button
                little
                title='удалить'
                onPress={() => {
                  this.props.onDelete(this.state.id);
                  this.props.navigation.goBack();
                }}
              />
            }
            <Button
              little={this.props.navigation.getParam('parameter', null) ? true : false}
              title='сохранить'
              onPress={() => {
                const parameter = {
                  id: this.state.id,
                  value: this.state.value,
                  name: 'температура',
                  routeName: 'TemperatureEditing',
                  time: this.state.time,
                  date: this.state.date,
                  feeling: this.state.feeling,
                };
                this.props.onSave(parameter);
                this.props.navigation.goBack()
              }}
            />
          </ButtonContainer>
        </ScrollView>
        <DateTimePicker
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this.state.mode === 'time' ? this.handleTimePicked : this.handleDatePicked}
          onCancel={this.hideDateTimePicker}
          mode={this.state.mode}
        />
      </View>
    );
  }
}

const mapStateToProps = state =>({
  feelingIcons: state.parameters.feelingIcons,
});

const mapDispatchToProps = dispatch =>({
  onSave: (values) => {
    dispatch(
      parameterAdd(values)
    )
  },
  onDelete: (id) => {
    dispatch(
      parameterDelete(id)
    )
  }
});

export default connect (
  mapStateToProps,
  mapDispatchToProps
)(TemperatureAddingScreen);