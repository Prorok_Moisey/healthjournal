// libraries
import React, {Component} from 'react';
import {View, ScrollView, TouchableWithoutFeedback} from 'react-native';
import styled from 'styled-components'
import {connect} from "react-redux";
import _ from "lodash";

// components
import Parameter from "../components/Parameter";
import {DARK_GRAY_COLOR, SECONDARY_THEME_COLOR} from "../styles";
import WeightIcon from "../components/icons/WeightIcon";
import PressureIcon from "../components/icons/PressureIcon";
import TemperatureIcon from "../components/icons/TemperatureIcon";
import SugarIcon from "../components/icons/SugarIcon";

const SectionTitle = styled.Text`
  height: 30;
  width: 100%;
  color: ${DARK_GRAY_COLOR};
  font-size: 14px;
  text-align-vertical: bottom;
  padding-left: 20;
  padding-bottom: 3;
  border-bottom-color: ${SECONDARY_THEME_COLOR};
  border-bottom-width: 2px;
`;

const InfoText = styled.Text`
  color: ${DARK_GRAY_COLOR};
  font-size: 18px;
  text-align: center;
`;

const Section = styled.View`
`;

const Time = styled.Text`
  color: ${DARK_GRAY_COLOR};
  font-size: 16px;
`;

class JournalScreen extends Component {
  render() {
    const groupedList = this.props.parameterList.length === 0 ? null : _.chain(this.props.parameterList).groupBy('date').toPairs().map(
      value => _.zipObject(['date', 'values'], value)
    ).value();
    const sortedList = this.props.parameterList.length === 0 ? null : _.sortBy(groupedList, function(e){return Date.parse(new Date(e.date))}).reverse();
    const mappedPostfix = {WeightEditing: 'кг', SugarEditing: 'ммоль', TemperatureEditing: '°C', PressureEditing: ''};
    const mappedIcon = {WeightEditing: WeightIcon, SugarEditing: SugarIcon, TemperatureEditing: TemperatureIcon, PressureEditing: PressureIcon};
    return (
      <View style={{ backgroundColor: 'white', alignItems: 'center', flex: 1 }}>
        <ScrollView
          style={{width: '100%', flex:1 }}
          contentContainerStyle={{alignItems: 'center'}}
        >
          <View style={{width: '90%'}}>
            { this.props.parameterList.length !== 0 ?
              sortedList.map(section => (
                <View>
                 <SectionTitle>{section.date}</SectionTitle>
                 <Section>
                   {
                     _.orderBy(section.values, ['time'],['desc']).map(item =>(
                       <Parameter
                         big
                         left={mappedIcon[item.routeName]}
                         main={item.value+mappedPostfix[item.routeName]}
                         secondary={item.name}
                         right={<Time>{item.time}</Time>}
                         onPress={() => this.props.navigation.navigate(item.routeName,{parameter: item})}
                       />
                     ))
                   }
                 </Section>
                </View>
              ))
              : <View style={{paddingTop: 190}}>
                  <TouchableWithoutFeedback
                    onPress={() => this.props.navigation.navigate('ParameterMain')}
                  >
                    <InfoText>У Вас сейчас нет сохранённых записей. Вы можете добавить их на экране ВВОД ДАННЫХ</InfoText>
                  </TouchableWithoutFeedback>
                </View>
            }
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state =>({
  parameterList: state.parameters.parameterList,
});

export default connect (
  mapStateToProps,
)(JournalScreen);