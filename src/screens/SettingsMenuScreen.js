import React, {Component} from 'react';
import {ScrollView, View, TouchableWithoutFeedback} from 'react-native';
import UserIcon from 'react-native-vector-icons/FontAwesome'

import {ICON_MAIN_COLOR, MAIN_TEXT_COLOR, MAIN_THEME_COLOR} from "../styles";
import styled from 'styled-components'
import BackIcon from "../components/icons/BackIcon";

const IconView = styled.View`
  width: 140;
  height: 140;
  border: 1px solid black;
  border-radius: 70;
  align-items: center;
  justify-content: center;
`;

const UserName = styled.Text`
  font-size: 16;
  color: ${MAIN_TEXT_COLOR};
`;

const Avatar = styled.View`
  height: 200;
  align-items: center;
  justify-content: space-around;
`;

const Menu = styled.View`
  background-color: ${MAIN_THEME_COLOR};
  width: 100%;
  flex: 1;
  padding: 0 30px;
  border-top-width: 1;
  border-top-color: black;
  justify-content: space-between;
`;

const MenuElement = styled.Text`
  font-size: 20;
  margin: 10px 0;
  color: ${MAIN_TEXT_COLOR};
`;

// const About = styled.Text`
//   padding: 20px 0;
//   font-size: 20;
//   color: ${MAIN_TEXT_COLOR};
//   border-top-width: 1;
//   border-top-color: black;
// `;

class SettingsMenuScreen extends Component {
  static navigationOptions = {
    headerTitle: 'НАСТРОЙКИ',
    headerLeft: <BackIcon/>,
    headerRight: <View/>,
    headerRightContainerStyle: {},
  };
  render() {
    const username='имя пользователя';
    return (
      <View style={{alignItems: 'center', flex: 1}}>
        <ScrollView
          style={{width: '100%', flex: 1}}
          contentContainerStyle={{flex: 1, alignItems: 'center'}}
        >
          <Avatar>
            <IconView>
              <UserIcon
                style={{margin:10}}
                name='user'
                size={120}
                color={ICON_MAIN_COLOR}
              />
            </IconView>
            <UserName>{username.toUpperCase()}</UserName>
          </Avatar>
          <Menu>
            <View>
              <TouchableWithoutFeedback
                onPress={() => this.props.navigation.navigate('SettingsUserInfo')}
              >
                <MenuElement>Учётная запись</MenuElement>
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback
                onPress={() => this.props.navigation.navigate('SettingsParameters')}
              >
                <MenuElement>Настройки ввода</MenuElement>
              </TouchableWithoutFeedback>
            </View>
            {/*<About>О программе</About>*/}
          </Menu>
        </ScrollView>
      </View>
    );
  }
}

export default SettingsMenuScreen