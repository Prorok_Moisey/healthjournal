// libraries
import React, {Component} from 'react';
import {ScrollView, Text, View} from 'react-native';
import {CheckBox} from 'react-native-elements';
import { connect } from 'react-redux'

// components
import Parameter from "../components/Parameter";
import WeightIcon from "../components/icons/WeightIcon";
import PressureIcon from "../components/icons/PressureIcon";
import TemperatureIcon from "../components/icons/TemperatureIcon";
import PlusIcon from "../components/icons/PlusIcon";
import SugarIcon from "../components/icons/SugarIcon";
import {DARK_GRAY_COLOR, MAIN_THEME_COLOR, SECONDARY_THEME_COLOR} from "../styles";
import BackIcon from "../components/icons/BackIcon";

// redux
import {parameterVisibilityChange} from "../state/reducers/parameters";

class SettingsParametersScreen extends Component {

  static navigationOptions = {
    headerTitle: 'НАСТРОЙКИ ВВОДА',
    headerLeft: <BackIcon/>,
    headerRight: <View/>,
    headerRightContainerStyle: {},
  };

  render() {
    return (
      <View style={{alignItems: 'center', flex: 1}}>
        <ScrollView
          style={{width: '100%', flex: 1}}
          contentContainerStyle={{alignItems: 'center'}}
        >
          <View style={{width: '90%'}}>
            <Parameter
              left={WeightIcon}
              main='вес'
              position='left'
              right={<CheckBox
                checked={this.props.weightEnabled}
                onPress={ () => this.props.onVisibilityChange(
                  {weightEnabled: !this.props.weightEnabled})
                }
                iconType='material-community'
                checkedIcon='checkbox-marked'
                uncheckedIcon='checkbox-blank-outline'
                checkedColor={DARK_GRAY_COLOR}
                uncheckedColor={DARK_GRAY_COLOR}
                size={28}
              />}
            />
            <Parameter
              left={PressureIcon}
              main='давление'
              position='left'
              right={<CheckBox
                checked={this.props.pressureEnabled}
                onPress={ () => this.props.onVisibilityChange(
                  {pressureEnabled: !this.props.pressureEnabled})
                }
                iconType='material-community'
                checkedIcon='checkbox-marked'
                uncheckedIcon='checkbox-blank-outline'
                checkedColor={DARK_GRAY_COLOR}
                uncheckedColor={DARK_GRAY_COLOR}
                size={28}
              />}
            />
            <Parameter
              left={TemperatureIcon}
              main='температура'
              position='left'
              right={<CheckBox
                checked={this.props.temperatureEnabled}
                onPress={ () => this.props.onVisibilityChange(
                  {temperatureEnabled: !this.props.temperatureEnabled})
                }
                iconType='material-community'
                checkedIcon='checkbox-marked'
                uncheckedIcon='checkbox-blank-outline'
                checkedColor={DARK_GRAY_COLOR}
                uncheckedColor={DARK_GRAY_COLOR}
                size={28}
              />}
            />
            <Parameter
              left={SugarIcon}
              main='уровень сахара'
              position='left'
              right={<CheckBox
                checked={this.props.sugarEnabled}
                onPress={ () => this.props.onVisibilityChange(
                  {sugarEnabled: !this.props.sugarEnabled})
                }
                iconType='material-community'
                checkedIcon='checkbox-marked'
                uncheckedIcon='checkbox-blank-outline'
                checkedColor={DARK_GRAY_COLOR}
                uncheckedColor={DARK_GRAY_COLOR}
                size={28}
              />}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state =>({
  weightEnabled: state.parameters.weightEnabled,
  pressureEnabled: state.parameters.pressureEnabled,
  temperatureEnabled: state.parameters.temperatureEnabled,
  sugarEnabled: state.parameters.sugarEnabled,
});

const mapDispatchToProps = dispatch =>({
  onVisibilityChange: (values) => {
    dispatch(
      parameterVisibilityChange(values)
    )
  }
});

export default connect (
  mapStateToProps,
  mapDispatchToProps
)(SettingsParametersScreen);
