// libraries
import React, {Component} from 'react';
import {ScrollView, View} from 'react-native';
import UserIcon from 'react-native-vector-icons/FontAwesome'
import {Formik} from 'formik';
import makeInputGreatAgain from "react-native-formik";
import { connect } from 'react-redux'

// components
import {ICON_MAIN_COLOR} from "../styles";
import styled from 'styled-components'
import Button from "../components/Button";
import UserInput from "../components/input/UserInput";
import BackIcon from "../components/icons/BackIcon";

// state
import {userUpdate} from "../state/reducers/user";

// constants
const Input = makeInputGreatAgain(UserInput);

const IconView = styled.View`
  width: 140;
  height: 140;
  border: 1px solid black;
  border-radius: 70;
  align-items: center;
  justify-content: center;
`;

const Avatar = styled.View`
  height: 200;
  align-items: center;
  justify-content: space-around;
`;

const ButtonContainer = styled.View`
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  padding: 35px 0;
`;

const Form = styled.View`
  height: 100%;
  width: 80%;
`;

class SettingsUserInfoScreen extends Component {
  static navigationOptions = {
    headerTitle: 'МОЯ УЧЁТНАЯ ЗАПИСЬ',
    headerLeft: <BackIcon/>,
    headerRight: <View/>,
    headerRightContainerStyle: {},
  };
  render() {
  const { firstName, lastName,  birthDate, sex, growth } = this.props;
    return (
      <View style={{alignItems: 'center', flex: 1}}>
        <ScrollView
          style={{width: '100%', flex: 1}}
          contentContainerStyle={{alignItems: 'center'}}
        >
          <Avatar>
            <IconView>
              <UserIcon
                style={{margin:10}}
                name='user'
                size={120}
                color={ICON_MAIN_COLOR}
              />
            </IconView>
          </Avatar>
          <Formik
            ref={form => {
              this.form = form;
            }}
            initialValues={{
              firstName: firstName,
              lastName: lastName,
              birthDate: birthDate,
              sex: sex,
              growth: growth,
            }}
            validateOnBlur={false}
            onSubmit={(values) => {
              this.props.onUserInfoSave(values);
            }}
          >
            {({
              values,
              errors,
              touched,
              onSubmit,
            }) => (
              <Form>
                <Input
                  name='firstName'
                  error={errors.firstName}
                  touched={touched.firstName}
                  placeholder='Имя'
                />
                <Input
                  name='lastName'
                  error={errors.lastName}
                  touched={touched.lastName}
                  placeholder='Фамилия'
                />
                <Input
                  name='birthDate'
                  error={errors.birthDate}
                  touched={touched.birthDate}
                  placeholder='Дата рождения'
                />
                <Input
                  name='sex'
                  error={errors.sex}
                  touched={touched.sex}
                  placeholder='Пол'
                />
                <Input
                  name='growth'
                  error={errors.growth}
                  touched={touched.growth}
                  placeholder='Рост'
                />
                <ButtonContainer>
                  <Button
                    title='Сохранить'
                    onPress={() => {
                      this.form.submitForm();
                      this.props.navigation.goBack();
                    }}
                  />
                </ButtonContainer>
              </Form>
            )}
          </Formik>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state =>({
  firstName: state.user.firstName,
  lastName: state.user.lastName,
  birthDate: state.user.birthDate,
  sex: state.user.sex,
  growth: state.user.growth,
});

const mapDispatchToProps = dispatch =>({
  onUserInfoSave: (values) => {
    dispatch(
      userUpdate(values)
    )
  }
});

export default connect (
  mapStateToProps,
  mapDispatchToProps
)(SettingsUserInfoScreen);