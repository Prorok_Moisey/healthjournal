// libraries
import React, {Component} from 'react';
import {ScrollView, View} from 'react-native';
import {connect} from "react-redux";

// components
import Parameter from "../components/Parameter";
import WeightIcon from "../components/icons/WeightIcon";
import PressureIcon from "../components/icons/PressureIcon";
import TemperatureIcon from "../components/icons/TemperatureIcon";
import PlusIcon from "../components/icons/PlusIcon";
import SugarIcon from "../components/icons/SugarIcon";

class MainScreen extends Component {
  render() {
    return (
      <View style={{alignItems: 'center', flex: 1}}>
        <ScrollView
          style={{width: '100%', flex: 1}}
          contentContainerStyle={{alignItems: 'center'}}
        >
          <View style={{width: '90%'}}>
            {this.props.weightEnabled &&
              <Parameter
                left={WeightIcon}
                main='вес'
                position='center'
                right={PlusIcon}
                onPress={() => this.props.navigation.navigate('WeightAdding')}
              />
            }
            {this.props.pressureEnabled &&
              <Parameter
                left={PressureIcon}
                main='давление'
                position='center'
                right={PlusIcon}
                onPress={() => this.props.navigation.navigate('PressureAdding')}
              />
            }
            {this.props.temperatureEnabled &&
              <Parameter
                left={TemperatureIcon}
                main='температура'
                position='center'
                right={PlusIcon}
                onPress={() => this.props.navigation.navigate('TemperatureAdding')}
              />
            }
            {this.props.sugarEnabled &&
              <Parameter
                left={SugarIcon}
                main='уровень сахара'
                position='center'
                right={PlusIcon}
                onPress={() => this.props.navigation.navigate('SugarAdding')}
              />
            }
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state =>({
  weightEnabled: state.parameters.weightEnabled,
  pressureEnabled: state.parameters.pressureEnabled,
  temperatureEnabled: state.parameters.temperatureEnabled,
  sugarEnabled: state.parameters.sugarEnabled,
});

export default connect (
  mapStateToProps
)(MainScreen);
